package com.example.caritorincon.basebootcamp;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;

public class ProcessNumberActivity extends AppCompatActivity {

    public static final String PROCESS_NUMBER_ACTIVITY = "ProcessNumberActivity";
    public static final String RESULT = "result";
    private int result;
    private int resultCode;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_process_number);

        processNumbers();
    }

    private void processNumbers() {
        Log.i(PROCESS_NUMBER_ACTIVITY, "Starting processNumbers");
        if (getIntent().hasExtra(ExerciseThreeActivity.ONE) && getIntent().hasExtra(ExerciseThreeActivity.TWO)) {
            result = getIntent().getIntExtra(ExerciseThreeActivity.ONE, 0) * getIntent().getIntExtra(ExerciseThreeActivity.TWO, 0);
            Log.i(PROCESS_NUMBER_ACTIVITY, "Result processNumbers : " + result);
            resultCode = RESULT_OK;

        } else {
            Log.e(PROCESS_NUMBER_ACTIVITY, "There are not numbers for processing : " + result);
            resultCode = RESULT_CANCELED;
            this.finish();
        }


    }

    public void onClickHomeResult(View v) {
        this.finish();
    }

    @Override
    public void finish() {
        Intent data = new Intent();
        data.putExtra(RESULT, result);
        setResult(resultCode, data);
        super.finish();
    }
}
