package com.example.caritorincon.basebootcamp.model;

import com.example.caritorincon.basebootcamp.R;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by carito on 10/19/2016.
 */

public class VersionModel {

    private static List<Version> list = new ArrayList<>();

    static {
        list.add(new Version("Jelly Bean", R.drawable.jellybean));
        list.add(new Version("KitKat", R.drawable.kitkat));
        list.add(new Version("Lollipop", R.drawable.lollipop));
        list.add(new Version("Marshmallow", R.drawable.marshmallow));
        list.add(new Version("Nougat", R.drawable.nougat));
    }

    public static List<Version> getList() {
        return list;
    }
}
