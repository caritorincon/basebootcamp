package com.example.caritorincon.basebootcamp;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

public class ProcessActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_process);

        showStrings();
    }

    private void showStrings() {
        TextView strOne = (TextView)findViewById(R.id.process_string_one);
        strOne.setText(getIntent().getStringExtra("str_one"));

        TextView strTwo = (TextView)findViewById(R.id.process_string_two);
        strTwo.setText(getIntent().getStringExtra("str_two"));

        TextView strThree = (TextView)findViewById(R.id.process_string_three);
        strThree.setText(getIntent().getStringExtra("str_three"));

        TextView check = (TextView)findViewById(R.id.process_check);
        check.setText("Checkbox en " + getIntent().getBooleanExtra("check" , false));
    }

    public void onClickHome(View v){
       this.finish();
    }
}
