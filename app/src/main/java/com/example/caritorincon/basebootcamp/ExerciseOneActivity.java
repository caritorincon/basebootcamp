package com.example.caritorincon.basebootcamp;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.CheckBox;
import android.widget.TextView;
import android.widget.Toast;

public class ExerciseOneActivity extends AppCompatActivity {

    private TextView stringOne , stringTwo ,stringThree;
    private CheckBox checkBox;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_exercise_one);
    }

    public void onClickProcess(View v){
        stringOne = (TextView)findViewById(R.id.string_one);
        stringTwo = (TextView)findViewById(R.id.string_two);
        stringThree = (TextView)findViewById(R.id.string_three);
        checkBox = (CheckBox)findViewById(R.id.checkBox);
        
        if (stringOne.getText().toString().trim().length() > 1 &&
            stringTwo.getText().toString().trim().length() > 1 &&
            stringThree.getText().toString().trim().length() > 1  ){
            process();
        }else{
            Toast.makeText(this , R.string.validation_msg , Toast.LENGTH_LONG ).show();
        }
    }

    private void process() {
        Intent intent = new Intent(this , ProcessActivity.class);

        intent.putExtra("str_one" , stringOne.getText().toString());
        intent.putExtra("str_two" , stringTwo.getText().toString());
        intent.putExtra("str_three" , stringThree.getText().toString());
        intent.putExtra("check" , checkBox.isChecked());

        startActivity(intent);
    }
}
