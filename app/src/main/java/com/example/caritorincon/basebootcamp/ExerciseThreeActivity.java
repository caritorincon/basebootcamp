package com.example.caritorincon.basebootcamp;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class ExerciseThreeActivity extends AppCompatActivity {

    public static final String ONE = "one";
    public static final String TWO = "two";
    static final int PROCESS_NUMBERS_REQUEST = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_exercise_three);
    }

    public void onClickProcessNumbers(View v) {

        EditText numberOne = (EditText) findViewById(R.id.number_one);
        EditText numberTwo = (EditText) findViewById(R.id.number_two);

        if (numberOne.getText().length() > 0 && numberTwo.getText().length() > 0) {
            process(Integer.valueOf(numberOne.getText().toString()), Integer.valueOf(numberTwo.getText().toString()));
        } else {
            Toast.makeText(this, R.string.validation_msg, Toast.LENGTH_LONG).show();
        }

    }

    private void process(int one, int two) {
        Log.i("ExerciseThreeActivity", "Sending info to ProcessNumberActivity");
        Intent intent = new Intent(this, ProcessNumberActivity.class);
        intent.putExtra(ONE, one);
        intent.putExtra(TWO, two);

        startActivityForResult(intent, PROCESS_NUMBERS_REQUEST);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == PROCESS_NUMBERS_REQUEST && resultCode == RESULT_OK) {
            Log.i("ExerciseThreeActivity", "Getting info from ProcessNumberActivity");
            TextView resultTextView = (TextView) findViewById(R.id.resultText);
            if (data.hasExtra(ProcessNumberActivity.RESULT)) {
                resultTextView.setText(String.valueOf(data.getIntExtra(ProcessNumberActivity.RESULT, 0)));
            }
        }

        super.onActivityResult(requestCode, resultCode, data);
    }
}
