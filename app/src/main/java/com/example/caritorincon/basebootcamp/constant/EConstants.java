package com.example.caritorincon.basebootcamp.constant;

/**
 * Created by carito on 10/18/2016.
 */

public enum EConstants {
    MONTH ("month");

    private String value;
    EConstants(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }
}
