package com.example.caritorincon.basebootcamp;

import android.app.Fragment;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.caritorincon.basebootcamp.model.VersionModel;


/**
 * Fragment that contains a list of android versions
 */
public class AndroidVersionFragment extends Fragment {

    public AndroidVersionFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View fragmentView = inflater.inflate(R.layout.fragment_android_version, container, false);
        loadVersions(fragmentView);
        return fragmentView;
    }


    private void loadVersions(View v) {
        RecyclerView versionRecyclerView = (RecyclerView) v.findViewById(R.id.versionList);
        VersionAdapter adapter = new VersionAdapter(VersionModel.getList());
        versionRecyclerView.setAdapter(adapter);
        versionRecyclerView.setLayoutManager(new LinearLayoutManager(v.getContext(), LinearLayoutManager.VERTICAL, false));
    }

}
