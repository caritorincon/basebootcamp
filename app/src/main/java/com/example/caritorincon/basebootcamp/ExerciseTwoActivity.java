package com.example.caritorincon.basebootcamp;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.example.caritorincon.basebootcamp.constant.EConstants;

public class ExerciseTwoActivity extends AppCompatActivity {

    private static String[] months = {"Enero" , "Febrero" , "Marzo" , "Abril" , "Mayo" , "Junio" , "Julio" , "Agosto" , "Septiembre" , "Octubre" , "Noviembre" , "Diciembre"};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_exercise_two);
        loadMonths();
    }

    private void loadMonths() {
        final ListView monthListView = (ListView)findViewById(R.id.monthsListView);
        ArrayAdapter<String> adapter = new ArrayAdapter<>(this , android.R.layout.simple_list_item_1 , months);
        monthListView.setAdapter(adapter);
        monthListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                showDetail(position);
            }
        });
    }

    private void showDetail(int position) {
        Log.i("ExerciseTwoActivity" , "Month selected pos : " + position);
        Intent intent = new Intent(ExerciseTwoActivity.this , MonthDetailActivity.class);
        intent.putExtra(EConstants.MONTH.getValue()  , months[position] );
        startActivity(intent);
    }
}
