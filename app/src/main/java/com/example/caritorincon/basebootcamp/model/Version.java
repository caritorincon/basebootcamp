package com.example.caritorincon.basebootcamp.model;

/**
 * Created by carito on 10/19/2016.
 */

public class Version {
    private String name;
    private int pathImagen;

    public Version(String name, int pathImagen) {
        this.name = name;
        this.pathImagen = pathImagen;
    }

    public String getName() {
        return name;
    }

    public int getPathImagen() {
        return pathImagen;
    }
}
