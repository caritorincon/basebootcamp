package com.example.caritorincon.basebootcamp;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.caritorincon.basebootcamp.model.Version;

import java.util.List;

/**
 * Created by carito on 10/19/2016.
 */

public class VersionAdapter extends RecyclerView.Adapter<VersionAdapter.VersionHolder> {

    private List<Version> versionList;

    public VersionAdapter(List<Version> versionList) {
        this.versionList = versionList;
    }

    @Override
    public VersionHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.version_item, parent, false);
        return new VersionHolder(itemView);
    }

    @Override
    public void onBindViewHolder(VersionHolder holder, int position) {
        holder.bind(versionList.get(position));
    }

    @Override
    public int getItemCount() {
        return versionList.size();
    }


    public class VersionHolder extends RecyclerView.ViewHolder {
        private ImageView image;
        private TextView name;

        public VersionHolder(View itemView) {
            super(itemView);
            image = (ImageView) itemView.findViewById(R.id.version_image);
            name = (TextView) itemView.findViewById(R.id.version_name);
        }

        void bind(Version version) {
            name.setText(version.getName());
            image.setImageResource(version.getPathImagen());
        }
    }
}
