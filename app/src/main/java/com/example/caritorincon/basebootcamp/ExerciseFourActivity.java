package com.example.caritorincon.basebootcamp;

import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

public class ExerciseFourActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_exercise_four);

        loadFragment();
    }

    private void loadFragment() {
        Log.i("ExerciseFourActivity", "Loading Fragment");
        FragmentManager manager = getFragmentManager();
        FragmentTransaction transaction = manager.beginTransaction();

        AndroidVersionFragment versionFragment = new AndroidVersionFragment();
        transaction.add(R.id.activity_exercise_four, versionFragment);
        transaction.commit();
    }
}
