package com.example.caritorincon.basebootcamp;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.example.caritorincon.basebootcamp.constant.EConstants;

public class MonthDetailActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_month_detail);

        if (getIntent().hasExtra(EConstants.MONTH.getValue())) {
            TextView month = (TextView) findViewById(R.id.monthTextView);
            month.setText(getString(R.string.month_detail) + " " + getIntent().getStringExtra(EConstants.MONTH.getValue()));
        } else {
            Log.e("MonthDetailActivity", "Month info not found");
        }
    }

    public void onClickHome(View v) {
        this.finish();
    }
}
