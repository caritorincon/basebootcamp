package com.example.caritorincon.basebootcamp;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void exercise0ne(View v) {
        Intent intent = new Intent(this , ExerciseOneActivity.class);
        startActivity(intent);
    }

    public void exerciseTwo(View v) {
        Intent intent = new Intent(this , ExerciseTwoActivity.class);
        startActivity(intent);
    }

    public void exerciseThree(View v) {
        Intent intent = new Intent(this, ExerciseThreeActivity.class);
        startActivity(intent);
    }

    public void exerciseFour(View v) {
        Intent intent = new Intent(this, ExerciseFourActivity.class);
        startActivity(intent);
    }
}
